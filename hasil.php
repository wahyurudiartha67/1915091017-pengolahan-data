<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <title>hasil</title>

    <style>
      body{
        background-image: url("https://www.phyramid.com/images/blog/making-phyramid-coms-procedurally-rendered-3d-header/header-background-indexed.png");
        background-size:cover;
      }
      .col-8{
        background-color:white;
      }
      .header{
          color:white;
          background-image: 
          url("https://designninjaz.com/wp-content/uploads/2013/10/headerImage2.jpg");
          text-align:center;
          padding: 40px;
          background-size:cover;
      }
      .footer{
        text-align:center;
        color:white;
      }
  </style>
  </head>
  
  <body>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8" style="border-style: outset;">
            <div class="header">
                <h2 style="font-weight:bold;text-shadow: 2px 2px 5px gray;">-  PENGOLAHAN DATA  -</h2>
            </div> <br>
            <?php
                $nama=$_POST["nama"];
                $mapel=$_POST["mapel"];
                $uts=$_POST["uts"];
                $uas=$_POST["uas"];
                $tugas=$_POST["tugas"];
                $total= ($_POST["uts"]*35/100)+($_POST["uas"]*50/100)+($_POST["tugas"]*15/100);

                if(($total)>=90 && ($total)<=100){
                    $grade="A";
                }
                if(($total)>70 && ($total)<90){
                    $grade="B";
                }
                if(($total)>50 && ($total)<70){
                    $grade="C";
                }
                if(($total)<=50){
                    $grade="D";
                }

            ?>
            <div class="alert alert-success">
            <h5>Hasil Pengolahan Data</h5><br>
            <?php
                echo "Nama Siswa/Siswi &nbsp; : $nama <br>";
                echo "Mata Pelajaran &nbsp; &nbsp; &nbsp; &nbsp;: $mapel<br>";
                echo "Nilai UTS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : $uts<br>";
                echo "Nilai UAS &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : $uas<br>";
                echo "Nilai Tugas &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: $tugas<br><br><hr>";
                echo "<strong>Nilai Total &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : $total</strong><br>";
                echo "<strong>Grade Nilai &nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : $grade</strong><br>";
            ?>
            </div>
        </div>
        <div class="footer"><br>&copy; 2021. Wahyu Rudiartha. All Rights Reserved.</div>
    </div>
    


    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
    -->
  </body>
</html>